package com.rdm.agrofw.utils;

import android.database.Cursor;

import com.rdm.api.annotations.ColumnDef;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robson Da Motta on 04/02/2016.
 */
public class ManagerCursor {

    public static <T> List<T> getObjectListOfType(Class<T> type) {
        return new ArrayList<T>();
    }

    public static Object getObjectOfClass(String className) {
        Class<?> classObject = null;
        Object object = null;
        try {
            classObject = Class.forName(className);

            object = classObject.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return object;
    }

    protected static Object getValueOfItemCursor(Cursor cursor, int index) {

        int collumType = cursor.getType(index);

        if (collumType == Cursor.FIELD_TYPE_INTEGER) {
            return cursor.getInt(index);
        } else if (collumType == Cursor.FIELD_TYPE_STRING) {
            return cursor.getString(index);

        } else if (collumType == Cursor.FIELD_TYPE_FLOAT) {
            return cursor.getDouble(index);
        } else {
            return null;
        }

    }

    public static Object cursorToObject(Cursor cursor, Class<?> tClass) {

        Object object = getObjectOfClass(tClass.getName());

        for (Field fieldObject : object.getClass().getDeclaredFields()) {
            fieldObject.setAccessible(true);
            for (Annotation annotationField : fieldObject.getAnnotations()) {

                String nome = "";
                String fieldNome = fieldObject.getName();

                if (annotationField instanceof com.rdm.api.annotations.ColumnDef) {
                    nome = ((ColumnDef) annotationField).nome();

                    for (String collumName : cursor.getColumnNames()) {
                        if (collumName.equalsIgnoreCase(nome)) {

                            Object value = getValueOfItemCursor(cursor, cursor.getColumnIndex(collumName));
                            try {
                                fieldObject.set(object, value);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } else if (annotationField instanceof com.rdm.api.annotations.EntityDef) {

                }
            }
        }

        return object;
    }
}
