package com.rdm.agrofw.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;

/**
 * Created by Robson Da Motta on 27/01/2016.
 */
public class ManagerJSON {


    public static JSONObject toJSON(String strJSON){
        JSONObject json = null;

        String keyJSON = getStringISO8859(strJSON.trim());

        try {
            json =new JSONObject(keyJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String getStringISO8859(String string){
        Charset utf8charset = Charset.forName("UTF-8");
        Charset iso88591charset = Charset.forName("ISO-8859-15");

        ByteBuffer inputBuffer = ByteBuffer.wrap(string.getBytes());

        // decode UTF-8
        CharBuffer data = utf8charset.decode(inputBuffer);

        // encode ISO-8559-1
        ByteBuffer outputBuffer = iso88591charset.encode(data);
        byte[] outputData = outputBuffer.array();

        return new String(outputData, Charset.forName("ISO-8859-15") );
    }


    public static String toCommandSQLInsert(JSONObject json) {
        Iterator<?> keys = json.keys();
        String command = "Insert into ";
        String collums = "(";
        String values = "values (";
        String separador = "";
        while (keys.hasNext()) {
            String key = (String) keys.next();
            try {
                if (json.get(key) instanceof String) {
                    if (key.equalsIgnoreCase("tableName")) {
                        command += json.getString(key);
                    } else {
                        collums += separador + key;
                        values += separador + "'" + json.getString(key) + "'";
                        separador = ",";
                    }

                } else if (json.get(key) instanceof Integer) {
                    collums += separador + key;
                    values += separador + String.valueOf(json.getInt(key));
                    separador = ",";
                } else if (json.get(key) instanceof Double) {
                    collums += separador + key;
                    values += separador + String.valueOf(json.getDouble(key));
                    separador = ",";
                }
                else if (json.get(key) instanceof Long) {
                    collums += separador + key;
                    values += separador + String.valueOf(json.getLong(key));
                    separador = ",";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        collums += ") ";
        values += ");";
        return command + collums + values;
    }
}
