package com.rdm.agrofw.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.rdm.agrofw.R;

/**
 * Created by Robson Da Motta on 05/01/2016.
 */
public class DBHelperBase extends SQLiteOpenHelper{

    private Context contextInstance;
    private boolean useJsonDump;

    public DBHelperBase(Context context) {
        super(context, context.getString(R.string.app_name), null,
                context.getResources().getInteger(R.integer.version_db));
        contextInstance = context;
        useJsonDump = context.getResources().getBoolean(R.bool.dump_json);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (isResetDataBase()){
            onResetDataBase(db);
        } else {
            onCreateDataBase(db);
            onPopulateDataBase(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (isResetDataBase()){
            onResetDataBase(db);
        }

    }

    public Context getContextInstance() {
        return contextInstance;
    }

    public boolean isUseJsonDump(){ return useJsonDump;}

    public boolean isResetDataBase(){
        return contextInstance.getResources().getBoolean(R.bool.reset_database);
    }

    protected void onResetDataBase(SQLiteDatabase db){

    }

    protected void onCreateDataBase(SQLiteDatabase db){

    }

    protected void onPopulateDataBase(SQLiteDatabase db){

    }
}
