package com.rdm.agrofw.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rdm.agrofw.R;
import com.rdm.agrofw.utils.ManagerJSON;

/**
 * Created by Robson Da Motta on 06/01/2016.
 */
public class DBHelper extends DBHelperBase {

    private static DBHelper mDBHelperInstance;

    protected DBHelper(Context context) {
        super(context);
    }

    public static DBHelper getInstance(Context context) {
        if (mDBHelperInstance == null) {
            mDBHelperInstance = new DBHelper(context);
        }
        if (mDBHelperInstance.isResetDataBase()) {
            mDBHelperInstance.onResetDataBase(mDBHelperInstance.getWritableDatabase());
        }
        return mDBHelperInstance;
    }

    @Override
    protected void onCreateDataBase(SQLiteDatabase db) {
        String[] commands = mDBHelperInstance.getContextInstance()
                .getResources().getStringArray(R.array.create_model_database);

        for (String command : commands) {
            db.execSQL(command);
        }
    }

    @Override
    protected void onResetDataBase(SQLiteDatabase db) {
        String[] commands = mDBHelperInstance.getContextInstance()
                .getResources().getStringArray(R.array.drop_model_database);

        for (String command : commands) {
            db.execSQL(command);
        }

        onCreateDataBase(db);
        onPopulateDataBase(db);
    }

    protected void populateTableFromDumpFile(Integer[] listResources, SQLiteDatabase db){
        for (int id = 0; id < listResources.length; id ++){
            String[] commands = mDBHelperInstance.getContextInstance()
                    .getResources().getStringArray(listResources[id]);

            for (String command : commands) {
                String commandInsert = command;
                if (isUseJsonDump()) {
                    commandInsert = ManagerJSON.toCommandSQLInsert(ManagerJSON.toJSON(command));
                }
                db.execSQL(commandInsert);
            }
        }

    }
    @Override
    protected void onPopulateDataBase(SQLiteDatabase db) {
        if (isUseJsonDump()){
            Integer[] tableResources = new Integer[]{
                    R.array.dump_json_classes,
                    R.array.dump_json_pathology,
                    R.array.dump_json_symptom,
                    R.array.dump_json_diagnostic};
            populateTableFromDumpFile(tableResources, db);
        }else {
            Integer[] tableResources = new Integer[]{
                    R.array.dump_sql_classes,
                    R.array.dump_sql_pathology,
                    R.array.dump_sql_symptom,
                    R.array.dump_sql_diagnostic};
            populateTableFromDumpFile(tableResources, db);
        }
    }

    protected Cursor getCursorOf(int resourceTableName) {
        Cursor cursor = null;
        SQLiteDatabase db = getReadableDatabase();

        String tableName = mDBHelperInstance.getContextInstance().getString(resourceTableName);
        cursor = db.query(tableName, null, null, null, null, null, null);
        return cursor;
    }

    protected Cursor getCursorOf(int resourceTableName, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        SQLiteDatabase db = getReadableDatabase();

        String tableName = mDBHelperInstance.getContextInstance().getString(resourceTableName);
        cursor = db.query(tableName, null, selection, selectionArgs, null, null, null);
        return cursor;
    }

    public Cursor getPathologysOf() {
        return getCursorOf(R.string.tabela_pathology);
    }

    public Cursor getPathologysOf(String selection, String[] selectionArgs) {
        return getCursorOf(R.string.tabela_pathology, selection, selectionArgs);
    }

    public Cursor getClassPathologysOf() {
        return getCursorOf(R.string.tabela_classe);
    }

    public Cursor getClassPathologysOf(String selection, String[] selectionArgs) {
        return getCursorOf(R.string.tabela_classe, selection, selectionArgs);
    }

    public Cursor getSymptomsOf() {
        return getCursorOf(R.string.tabela_symptom);
    }

    public Cursor getSymptomsOf(String selection, String[] selectionArgs) {
        return getCursorOf(R.string.tabela_symptom);
    }

    public Cursor getDiagnosticsOf() {
        return getCursorOf(R.string.tabela_diagnostic);
    }

    public Cursor getDiagnosticsOf(String selection, String[] selectionArgs) {
        return getCursorOf(R.string.tabela_diagnostic);
    }

    public Cursor getComplementsOf() {
        return getCursorOf(R.string.tabela_complement);
    }

    public Cursor getComplementsOf(String selection, String[] selectionArgs) {
        return getCursorOf(R.string.tabela_complement);
    }
}
