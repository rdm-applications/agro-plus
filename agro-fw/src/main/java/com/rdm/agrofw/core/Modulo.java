package com.rdm.agrofw.core;

import android.content.Context;

import com.rdm.agrofw.database.DBHelper;
import com.rdm.api.application.ModuloApi;
import com.rdm.api.entidades.ClassePatologiaBase;
import com.rdm.api.entidades.ComplementoBase;
import com.rdm.api.entidades.DiagnosticoBase;
import com.rdm.api.entidades.DoencaBase;
import com.rdm.api.entidades.SintomaBase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robson Da Motta on 06/01/2016.
 */
public class Modulo implements Serializable{


    protected Context mContext;
    protected DBHelper dbHelper;
    protected ModuloApi moduloApi;

    public static Modulo newModulo() {
        return new Modulo();
    }

    public void inicializar(Context context) {
        mContext = context;
        dbHelper = DBHelper.getInstance(mContext);
        moduloApi = ModuloApi.build();
    }

    public void reset(){
        if (moduloApi.isPrepared()){
            for (DoencaBase pathology: moduloApi.getListPathologys()) {
                pathology.getClasse().setPeso(0);
            }
        }
    }
    protected void initializeListPathologys() {

        ArrayList<DoencaBase> pathologyList = new ArrayList<>();

        for (ClassePatologiaBase classPathologyItem : ManagerSearchDB.
                getClassPathologysOf(dbHelper)) {
            ArrayList<DoencaBase> pathologyListFromClass = ManagerSearchDB.
                    getPathologysFromFromForeignKey(dbHelper, classPathologyItem.getIdClasse());

            for (DoencaBase pathologyItem : pathologyListFromClass) {
                ArrayList<SintomaBase> symptomList = ManagerSearchDB.
                        getSymptomsFromForeignKey(dbHelper, pathologyItem.getIdPathology());

                ArrayList<DiagnosticoBase> diagnosticList = ManagerSearchDB.
                        getDiagnosticsFromForeignKey(dbHelper, pathologyItem.getIdPathology());

                for (DiagnosticoBase diagnosticItem : diagnosticList) {
                    ArrayList<ComplementoBase> complementList = ManagerSearchDB.
                            getComplementsFromForeignKey(dbHelper,
                                    diagnosticItem.getIdDiagnostico());
                    pathologyItem.setComplementos(complementList);
                }
                pathologyItem.setClasse(classPathologyItem);
                pathologyItem.setDiagnosticos(diagnosticList);
                pathologyItem.setSintomas(symptomList);
                pathologyList.add(pathologyItem);
            }
        }
        moduloApi.prepare(pathologyList);
    }

    protected void initializeList() {
        initializeListPathologys();
    }

    public void prepare() {
        initializeList();
        //moduloApi.callGarbage();
        return;
    }

    public ArrayList<SintomaBase> getGroupSymptom(int group){
       if (!moduloApi.isPrepared()) {
           new Throwable("Api don't prepared!");
       }
       return moduloApi.genereteGroupSymptoms(group);
    }

    public List<DoencaBase> getListPathologys(){
        return moduloApi.getListPathologys();
    }

    public static String showDetail(Object object){

        if (!(object instanceof DoencaBase)){
            return "";
        }

        DoencaBase pathology = (DoencaBase) object;
        return pathology.getPathologyDescription();
    }

    public static String showDetail(ArrayList<?> arrayList){
        StringBuilder sb = new StringBuilder();
        if (arrayList.isEmpty()){
            return sb.toString();
        }

        for (int index = 0; index < arrayList.size(); index++){

            if (arrayList.get(index) instanceof DoencaBase){

               sb.append(((DoencaBase) arrayList.get(index)).getPathologyDescription() ).append("\n");
            } else if (arrayList.get(index) instanceof  SintomaBase){

                sb.append(((SintomaBase) arrayList.get(index)).getSintoma() ).append("\n");
            } else if (arrayList.get(index) instanceof  DiagnosticoBase) {

                sb.append(((DiagnosticoBase) arrayList.get(index)).getDiagnostico() ).append("\n");
            }
        }
        return  sb.toString();
    }
}
