package com.rdm.agrofw.core;

import android.database.Cursor;

import com.rdm.agrofw.database.DBHelper;
import com.rdm.agrofw.utils.ManagerCursor;
import com.rdm.api.entidades.ClassePatologiaBase;
import com.rdm.api.entidades.ComplementoBase;
import com.rdm.api.entidades.DiagnosticoBase;
import com.rdm.api.entidades.DoencaBase;
import com.rdm.api.entidades.SintomaBase;

import java.util.ArrayList;

/**
 * Created by Robson Da Motta on 04/02/2016.
 */
public class ManagerSearchDB {

    public static ArrayList<DoencaBase> getPathologysOf(DBHelper dbInstance) {
        ArrayList<DoencaBase> pathologyList = new ArrayList<>();
        Cursor cursorPathologys = dbInstance.getPathologysOf();
        while (cursorPathologys.moveToNext()) {
            DoencaBase pathology = ManagerSearchDB.getPathology(cursorPathologys, DoencaBase.class);
            pathologyList.add(pathology);
        }
        if (cursorPathologys != null) {
            cursorPathologys.close();
        }
        return pathologyList;
    }

    public static ArrayList<DoencaBase> getPathologysFromId(DBHelper dbInstance, int id) {
        ArrayList<DoencaBase> pathologyList = new ArrayList<>();
        Cursor cursorPathologys = dbInstance.getPathologysOf("_id=?", new  String[] {String.valueOf(id)});
        while (cursorPathologys.moveToNext()) {
            DoencaBase pathology = ManagerSearchDB.getPathology(cursorPathologys, DoencaBase.class);
            pathologyList.add(pathology);
        }
        if (cursorPathologys != null) {
            cursorPathologys.close();
        }
        return pathologyList;
    }

    public static ArrayList<DoencaBase> getPathologysFromFromForeignKey(DBHelper dbInstance, int foreignKeyClassPathology) {
        ArrayList<DoencaBase> pathologyList = new ArrayList<>();
        Cursor cursorPathologys = dbInstance.getPathologysOf("idClasse=?", new  String[] {String.valueOf(foreignKeyClassPathology)});
        while (cursorPathologys.moveToNext()) {
            DoencaBase pathology = ManagerSearchDB.getPathology(cursorPathologys, DoencaBase.class);
            pathologyList.add(pathology);
        }
        if (cursorPathologys != null) {
            cursorPathologys.close();
        }
        return pathologyList;
    }

    protected static DoencaBase getPathology(Cursor cursor, Class<?> tClass) {
        return (DoencaBase) ManagerCursor.cursorToObject(cursor, tClass);
    }

    public static ArrayList<ClassePatologiaBase> getClassPathologysOf(DBHelper dbInstance) {
        ArrayList<ClassePatologiaBase> classPathologysList = new ArrayList<>();
        Cursor cursorClassPathologys = dbInstance.getClassPathologysOf();
        try {
            while (cursorClassPathologys.moveToNext()) {
                ClassePatologiaBase classPathologys = ManagerSearchDB.getClassPathology(cursorClassPathologys, ClassePatologiaBase.class);
                classPathologysList.add(classPathologys);
            }
        } finally {
            if (cursorClassPathologys != null) {
                cursorClassPathologys.close();
            }
        }
        return classPathologysList;
    }

    public static ArrayList<ClassePatologiaBase> getClassPathologyFromId(DBHelper dbInstance, int id) {
        ArrayList<ClassePatologiaBase> classPathologysList = new ArrayList<>();
        Cursor cursorClassPathologys = dbInstance.getClassPathologysOf("_id", new  String[] {String.valueOf(id)});
        try {
            while (cursorClassPathologys.moveToNext()) {
                ClassePatologiaBase classPathologys = ManagerSearchDB.getClassPathology(cursorClassPathologys, ClassePatologiaBase.class);
                classPathologysList.add(classPathologys);
            }
        } finally {
            if (cursorClassPathologys != null) {
                cursorClassPathologys.close();
            }
        }
        return classPathologysList;
    }

    protected static ClassePatologiaBase getClassPathology(Cursor cursor, Class<?> tClass) {
        return (ClassePatologiaBase) ManagerCursor.cursorToObject(cursor, tClass);
    }

    public static ArrayList<SintomaBase> getSymptomsOf(DBHelper dbInstance) {
        ArrayList<SintomaBase> symptomsList = new ArrayList<>();
        Cursor cursorSymptoms = dbInstance.getSymptomsOf();
        try {
            while (cursorSymptoms.moveToNext()) {
                SintomaBase symptom = ManagerSearchDB.getSymptom(cursorSymptoms, SintomaBase.class);
                symptomsList.add(symptom);
            }
        } finally {
            if (cursorSymptoms != null) {
                cursorSymptoms.close();
            }
        }
        return symptomsList;
    }

    public static ArrayList<SintomaBase> getSymptomsFromId(DBHelper dbInstance, int id) {
        ArrayList<SintomaBase> symptomsList = new ArrayList<>();
        Cursor cursorSymptoms = dbInstance.getSymptomsOf("_id=?", new  String[] {String.valueOf(id)});
        try {
            while (cursorSymptoms.moveToNext()) {
                SintomaBase symptom = ManagerSearchDB.getSymptom(cursorSymptoms, SintomaBase.class);
                symptomsList.add(symptom);
            }
        } finally {
            if (cursorSymptoms != null) {
                cursorSymptoms.close();
            }
        }
        return symptomsList;
    }

    public static ArrayList<SintomaBase> getSymptomsFromForeignKey(DBHelper dbInstance, int foreignKeyPathology) {
        ArrayList<SintomaBase> symptomsList = new ArrayList<>();
        Cursor cursorSymptoms = dbInstance.getSymptomsOf("symptomsPathology=?", new  String[] {String.valueOf(foreignKeyPathology)});
        try {
            while (cursorSymptoms.moveToNext()) {
                SintomaBase symptom = ManagerSearchDB.getSymptom(cursorSymptoms, SintomaBase.class);
                symptomsList.add(symptom);
            }
        } finally {
            if (cursorSymptoms != null) {
                cursorSymptoms.close();
            }
        }
        return symptomsList;
    }

    protected static SintomaBase getSymptom(Cursor cursor, Class<?> tClass) {
        return (SintomaBase) ManagerCursor.cursorToObject(cursor, tClass);
    }

    public static ArrayList<DiagnosticoBase> getDiagnosticsOf(DBHelper dbInstance) {
        ArrayList<DiagnosticoBase> diagnosticsList = new ArrayList<>();
        Cursor cursorDiagnostic = dbInstance.getDiagnosticsOf();
        try {
            while (cursorDiagnostic.moveToNext()) {
                DiagnosticoBase diagnostic = ManagerSearchDB.getDiagnostic(cursorDiagnostic, DiagnosticoBase.class);
                diagnosticsList.add(diagnostic);
            }
        } finally {
            if (cursorDiagnostic != null) {
                cursorDiagnostic.close();
            }
        }
        return diagnosticsList;
    }

    public static ArrayList<DiagnosticoBase> getDiagnosticsFromId(DBHelper dbInstance, int id) {
        ArrayList<DiagnosticoBase> diagnosticsList = new ArrayList<>();
        Cursor cursorDiagnostic = dbInstance.getDiagnosticsOf("_id=?", new  String[] {String.valueOf(id)});
        try {
            while (cursorDiagnostic.moveToNext()) {
                DiagnosticoBase diagnostic = ManagerSearchDB.getDiagnostic(cursorDiagnostic, DiagnosticoBase.class);
                diagnosticsList.add(diagnostic);
            }
        } finally {
            if (cursorDiagnostic != null) {
                cursorDiagnostic.close();
            }
        }
        return diagnosticsList;
    }

    public static ArrayList<DiagnosticoBase> getDiagnosticsFromForeignKey(DBHelper dbInstance, int foreignKeySymptom) {
        ArrayList<DiagnosticoBase> diagnosticsList = new ArrayList<>();
        Cursor cursorDiagnostic = dbInstance.getDiagnosticsOf("diagnosticPathology=?", new  String[] {String.valueOf(foreignKeySymptom)});
        try {
            while (cursorDiagnostic.moveToNext()) {
                DiagnosticoBase diagnostic = ManagerSearchDB.getDiagnostic(cursorDiagnostic, DiagnosticoBase.class);
                diagnosticsList.add(diagnostic);
            }
        } finally {
            if (cursorDiagnostic != null) {
                cursorDiagnostic.close();
            }
        }
        return diagnosticsList;
    }

    protected static DiagnosticoBase getDiagnostic(Cursor cursor, Class<?> tClass) {
        return (DiagnosticoBase) ManagerCursor.cursorToObject(cursor, tClass);
    }

    public static ArrayList<ComplementoBase> getComplementsOf(DBHelper dbInstance) {
        ArrayList<ComplementoBase> complementsList = new ArrayList<>();
        Cursor cursorComplements = dbInstance.getComplementsOf();
        try {
            while (cursorComplements.moveToNext()) {
                ComplementoBase complement = ManagerSearchDB.getComplement(cursorComplements, ComplementoBase.class);
                complementsList.add(complement);
            }
        } finally {
            if (cursorComplements != null) {
                cursorComplements.close();
            }
        }
        return complementsList;
    }

    public static ArrayList<ComplementoBase> getComplementsFromId(DBHelper dbInstance, int id) {
        ArrayList<ComplementoBase> complementsList = new ArrayList<>();
        Cursor cursorComplements = dbInstance.getComplementsOf("_id=?", new  String[] {String.valueOf(id)});
        try {
            while (cursorComplements.moveToNext()) {
                ComplementoBase complement = ManagerSearchDB.getComplement(cursorComplements, ComplementoBase.class);
                complementsList.add(complement);
            }
        } finally {
            if (cursorComplements != null) {
                cursorComplements.close();
            }
        }
        return complementsList;
    }

    public static ArrayList<ComplementoBase> getComplementsFromForeignKey(DBHelper dbInstance, int foreignKeyDiagnostic) {
        ArrayList<ComplementoBase> complementsList = new ArrayList<>();
        Cursor cursorComplements = dbInstance.getComplementsOf("complementDiagnostic=?", new  String[] {String.valueOf(foreignKeyDiagnostic)});
        try {
            while (cursorComplements.moveToNext()) {
                ComplementoBase complement = ManagerSearchDB.getComplement(cursorComplements, ComplementoBase.class);
                complementsList.add(complement);
            }
        } finally {
            if (cursorComplements != null) {
                cursorComplements.close();
            }
        }
        return complementsList;
    }

    protected static ComplementoBase getComplement(Cursor cursor, Class<?> tClass) {
        return (ComplementoBase) ManagerCursor.cursorToObject(cursor, tClass);
    }
}
