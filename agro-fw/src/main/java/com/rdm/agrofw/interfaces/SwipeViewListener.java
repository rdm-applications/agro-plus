package com.rdm.agrofw.interfaces;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Robson Da Motta on 17/02/2016.
 */
public interface SwipeViewListener {

    boolean doSwipe(int position);
    void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reversePositions);
    void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reversePositions);

}
