package com.rdm.agrofw.interfaces;

import android.view.MenuItem;

/**
 * Created by Robson Da Motta on 09/03/2016.
 */
public interface ItemMenuToolbarListener {
    boolean onItemMenuToolbarClick(MenuItem menuItem, Object objectItem);
}
