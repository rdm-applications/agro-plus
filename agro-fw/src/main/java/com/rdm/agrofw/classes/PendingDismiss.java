package com.rdm.agrofw.classes;

import android.support.annotation.NonNull;
import android.view.View;

/**
 * Created by Robson Da Motta on 17/02/2016.
 */
public class PendingDismiss implements Comparable<PendingDismiss> {
    public int position;
    public View view;

    public PendingDismiss(int position, View view) {
        this.position = position;
        this.view = view;
    }

    @Override
    public int compareTo(@NonNull PendingDismiss other) {
        return other.position - position;
    }
}