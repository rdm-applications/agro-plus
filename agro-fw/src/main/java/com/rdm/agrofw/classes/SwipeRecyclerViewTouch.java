package com.rdm.agrofw.classes;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.os.Build;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ListView;

import com.rdm.agrofw.interfaces.SwipeViewListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Robson Da Motta on 17/02/2016.
 */
public class SwipeRecyclerViewTouch implements RecyclerView.OnItemTouchListener {
    private int mSlop;
    private int mMinFlingVelocity;
    private int mMaxFlingVelocity;
    private long mAnimationTime;
    private RecyclerView mInternalRecyclerView;
    private SwipeViewListener mSwipeViewListener;
    private int mViewWidth = 1; // 1 and not 0 to prevent dividing by zero

    private List<PendingDismiss> mPendingDismisses = new ArrayList<>();
    private int mDismissAnimationRefCount = 0;
    private float mAlpha;
    private float mDownX;
    private float mDownY;
    private boolean mSwiping;
    private int mSwipingSlop;

    private VelocityTracker mVelocityTracker;
    private int mDownPosition;
    private int mAnimatingPosition = ListView.INVALID_POSITION;
    private View mDownView;
    private boolean mPaused;
    private float mFinalDelta;

    public SwipeRecyclerViewTouch(RecyclerView recyclerView, SwipeViewListener callback) {
        this.mInternalRecyclerView = recyclerView;
        this.mSwipeViewListener = callback;

        initializePropertys();
    }

    protected void initializePropertys() {

        ViewConfiguration viewConfigurationc = ViewConfiguration.get(mInternalRecyclerView.getContext());

        this.mSlop = viewConfigurationc.getScaledTouchSlop();
        this.mMinFlingVelocity = viewConfigurationc.getScaledMinimumFlingVelocity() * 16;
        this.mMaxFlingVelocity = viewConfigurationc.getScaledMaximumFlingVelocity();

        this.mAnimationTime = mInternalRecyclerView.getContext().getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        mInternalRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                setEnabled(newState != RecyclerView.SCROLL_STATE_DRAGGING);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            }
        });
    }

    public void setEnabled(boolean enabled) {
        mPaused = !enabled;
    }

    private boolean handleTouchEvent(MotionEvent motionEvent) {
        if (mViewWidth < 2) {
            mViewWidth = mInternalRecyclerView.getWidth();
        }

        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_DOWN: {
                if (mPaused) {
                    break;
                }

                // Find the child view that was touched (perform a hit test)
                Rect rect = new Rect();
                int childCount = mInternalRecyclerView.getChildCount();
                int[] listViewCoords = new int[2];
                mInternalRecyclerView.getLocationOnScreen(listViewCoords);

                int x = (int) motionEvent.getRawX() - listViewCoords[0];
                int y = (int) motionEvent.getRawY() - listViewCoords[1];
                View child;
                for (int i = 0; i < childCount; i++) {
                    child = mInternalRecyclerView.getChildAt(i);
                    child.getHitRect(rect);
                    if (rect.contains(x, y)) {
                        mDownView = child;
                        break;
                    }
                }

                if (mDownView != null && mAnimatingPosition != mInternalRecyclerView.getChildPosition(mDownView)) {
                    mAlpha = mDownView.getAlpha();
                    mDownX = motionEvent.getRawX();
                    mDownY = motionEvent.getRawY();
                    mDownPosition = mInternalRecyclerView.getChildPosition(mDownView);
                    if (mSwipeViewListener.doSwipe(mDownPosition)) {
                        mVelocityTracker = VelocityTracker.obtain();
                        mVelocityTracker.addMovement(motionEvent);
                    } else {
                        mDownView = null;
                    }
                }
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                if (mVelocityTracker == null) {
                    break;
                }

                if (mDownView != null && mSwiping) {
                    showAnimateCancelView();
                }

                cancelActionMotionEvent();
                break;
            }

            case MotionEvent.ACTION_UP: {
                if (mVelocityTracker == null) {
                    break;
                }

                mFinalDelta = motionEvent.getRawX() - mDownX;
                mVelocityTracker.addMovement(motionEvent);
                mVelocityTracker.computeCurrentVelocity(1000);
                float velocityX = mVelocityTracker.getXVelocity();
                float absVelocityX = Math.abs(velocityX);
                float absVelocityY = Math.abs(mVelocityTracker.getYVelocity());
                boolean dismiss = false;
                boolean dismissRight = false;
                if (Math.abs(mFinalDelta) > mViewWidth / 2 && mSwiping) {
                    dismiss = true;
                    dismissRight = mFinalDelta > 0;
                } else if (mMinFlingVelocity <= absVelocityX && absVelocityX <= mMaxFlingVelocity
                        && absVelocityY < absVelocityX && mSwiping) {
                    // dismiss only if flinging in the same direction as dragging
                    dismiss = (velocityX < 0) == (mFinalDelta < 0);
                    dismissRight = mVelocityTracker.getXVelocity() > 0;
                }
                if (dismiss && mDownPosition != mAnimatingPosition && mDownPosition != ListView.INVALID_POSITION) {
                    final View downView = mDownView; // mDownView gets null'd before animation ends
                    final int downPosition = mDownPosition;
                    ++mDismissAnimationRefCount;
                    mAnimatingPosition = mDownPosition;

                    showAnimateDismissView(downView, downPosition, dismissRight);
                } else {
                    showAnimateCancelView();
                }
                cancelActionMotionEvent();
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                if (mVelocityTracker == null || mPaused) {
                    break;
                }
                return actionMoviment(motionEvent);

            }
        }

        return false;
    }

    protected boolean actionMoviment(MotionEvent motionEvent) {
        mVelocityTracker.addMovement(motionEvent);
        float deltaX = motionEvent.getRawX() - mDownX;
        float deltaY = motionEvent.getRawY() - mDownY;
        boolean isSwiping = (!mSwiping && Math.abs(deltaX) > mSlop && Math.abs(deltaY) < Math.abs(deltaX) / 2);
        mSwiping = isSwiping;
        mSwipingSlop = (deltaX > 0 ? mSlop : -mSlop);

        if (mSwiping) {
            mDownView.setTranslationX(deltaX - mSwipingSlop);
            mDownView.setAlpha(Math.max(0f, Math.min(mAlpha,
                    mAlpha * (1f - Math.abs(deltaX) / mViewWidth))));

        }
        return mSwiping;
    }

    protected void cancelActionMotionEvent() {

        mVelocityTracker.recycle();
        mVelocityTracker = null;
        mDownX = 0;
        mDownY = 0;
        mDownView = null;
        mDownPosition = ListView.INVALID_POSITION;
        mSwiping = false;
    }

    protected void showAnimateDismissView(final View downView, final int downPosition, boolean animationToRight) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDownView.animate()
                    .translationX(animationToRight ? mViewWidth : -mViewWidth)
                    .alpha(0)
                    .setDuration(mAnimationTime)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            performDismiss(downView, downPosition);
                        }
                    });
        }
    }

    protected void showAnimateCancelView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDownView.animate()
                    .translationX(0)
                    .alpha(mAlpha)
                    .setDuration(mAnimationTime)
                    .setListener(null);
        }
    }

    private void performDismiss(final View dismissView, final int dismissPosition) {

        final ViewGroup.LayoutParams lp = dismissView.getLayoutParams();
        final int originalLayoutParamsHeight = lp.height;
        final int originalHeight = dismissView.getHeight();

        ValueAnimator animator = ValueAnimator.ofInt(originalHeight, 1).setDuration(mAnimationTime);

        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                --mDismissAnimationRefCount;
                if (mDismissAnimationRefCount == 0) {
                    // No active animations, process all pending dismisses.
                    // Sort by descending position
                    Collections.sort(mPendingDismisses);

                    int[] dismissPositions = new int[mPendingDismisses.size()];
                    for (int i = mPendingDismisses.size() - 1; i >= 0; i--) {
                        dismissPositions[i] = mPendingDismisses.get(i).position;
                    }

                    if (mFinalDelta > 0) {
                        mSwipeViewListener.onDismissedBySwipeRight(mInternalRecyclerView, dismissPositions);
                    } else {
                        mSwipeViewListener.onDismissedBySwipeLeft(mInternalRecyclerView, dismissPositions);
                    }

                    // Reset mDownPosition to avoid MotionEvent.ACTION_UP trying to start a dismiss
                    // animation with a stale position
                    mDownPosition = ListView.INVALID_POSITION;

                    ViewGroup.LayoutParams lp;
                    for (PendingDismiss pendingDismiss : mPendingDismisses) {
                        // Reset view presentation
                        pendingDismiss.view.setAlpha(mAlpha);
                        pendingDismiss.view.setTranslationX(0);

                        lp = pendingDismiss.view.getLayoutParams();
                        lp.height = originalLayoutParamsHeight;

                        pendingDismiss.view.setLayoutParams(lp);
                    }

                    // Send a cancel event
                    long time = SystemClock.uptimeMillis();
                    MotionEvent cancelEvent = MotionEvent.obtain(time, time,
                            MotionEvent.ACTION_CANCEL, 0, 0, 0);
                    mInternalRecyclerView.dispatchTouchEvent(cancelEvent);

                    mPendingDismisses.clear();
                    mAnimatingPosition = ListView.INVALID_POSITION;
                }
            }
        });

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                lp.height = (Integer) valueAnimator.getAnimatedValue();
                dismissView.setLayoutParams(lp);
            }
        });

        mPendingDismisses.add(new PendingDismiss(dismissPosition, dismissView));
        animator.start();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return handleTouchEvent(e);
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        handleTouchEvent(e);
    }
}