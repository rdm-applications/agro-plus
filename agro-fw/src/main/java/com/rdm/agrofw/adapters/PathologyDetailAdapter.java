package com.rdm.agrofw.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rdm.agrofw.R;
import com.rdm.api.entidades.DoencaBase;

import java.util.List;

/**
 * Created by Robson Da Motta on 09/03/2016.
 */
public class PathologyDetailAdapter extends AgroAdapter {

    public PathologyDetailAdapter(List<DoencaBase> listPathologys) {
        super(listPathologys);
    }

    @Override
    public AgroAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        CardView mCardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fw_card_view_image_text, viewGroup, false);

        ViewHolder vh = new ViewHolder(mCardView);
        return vh;
    }

    @Override
    public void onBindViewHolder(AgroAdapter.ViewHolder viewHolder, int position) {

        //Image icon of class pathology
        ImageView iconImageClassPathology = (ImageView) viewHolder.mCard.findViewById(R.id.fwimgIconCardViewImageText);
        //Image background pathology
        ImageView backgroundImagePathology = (ImageView) viewHolder.mCard.findViewById(R.id.fwimgIconCardViewImageText);
        //Text title pathology
        TextView mTextViewPathologyPopularName = (TextView) viewHolder.mCard.findViewById(R.id.fwtvTitleCardViewImageText);
        //Text description pathology
        TextView mTextViewDescPathology = (TextView) viewHolder.mCard.findViewById(R.id.fwtvDescrCardViewImageText);
        //pathology index position
        final DoencaBase pathology = (DoencaBase) getItem(position);
        //Toolbar of menu card
        Toolbar toolbar = (Toolbar) viewHolder.mCard.findViewById(R.id.fwvtbCardViewImageText);
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.menu_detail_toolbar_card_view);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (getOnItemMenuToolbarListener() != null) {
                    return getOnItemMenuToolbarListener().onItemMenuToolbarClick(item, pathology);
                } else {
                    return false;
                }
            }
        });

        mTextViewPathologyPopularName.setText(pathology.getPathologyPopularName());
        mTextViewDescPathology.setText(pathology.getPathologyDescription());

        viewHolder.mCard.setTag(R.string.agro_fw_TAG, pathology);
    }

}
