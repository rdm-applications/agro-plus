package com.rdm.agrofw.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.rdm.agrofw.R;
import com.rdm.api.entidades.SintomaBase;

import java.util.List;

/**
 * Created by Robson Da Motta on 25/04/2016.
 */
public class SymptomAdapter extends AgroAdapter{


    public SymptomAdapter(List<SintomaBase> listSymptons) {
        super(listSymptons);
    }

    @Override
    public AgroAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        CardView mCardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fw_card_view_symptom_detail, viewGroup, false);

        AgroAdapter.ViewHolder vh = new AgroAdapter.ViewHolder(mCardView);
        return vh;
    }

    @Override
    public void onBindViewHolder(AgroAdapter.ViewHolder viewHolder, int position) {

        CheckBox checkBoxSymptom = (CheckBox) viewHolder.mCard.findViewById(R.id.fwCheckBoxSelect);

        final SintomaBase symptom = (SintomaBase) getItem(position);
        //Toolbar of menu card
        Toolbar toolbar = (Toolbar) viewHolder.mCard.findViewById(R.id.fwvtbCardViewSymptomDetail);
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.menu_detail_toolbar_card_view);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (getOnItemMenuToolbarListener() != null) {
                    return getOnItemMenuToolbarListener().onItemMenuToolbarClick(item, symptom);
                } else {
                    return false;
                }
            }
        });

        checkBoxSymptom.setText(symptom.getSintoma().toString());
        viewHolder.mCard.setTag(R.string.agro_fw_TAG, symptom);
    }
}
