package com.rdm.agrofw.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.rdm.agrofw.interfaces.ItemMenuToolbarListener;

import java.util.List;

/**
 * Created by Robson Da Motta on 16/02/2016.
 */
public class AgroAdapter extends RecyclerView.Adapter<AgroAdapter.ViewHolder> {

    private ItemMenuToolbarListener itemMenuToolbarListener;

    private List<?> mListItem;

    public AgroAdapter(List<?> mList) {
        this.mListItem = mList;
    }

    public Object getItem(int position){
        return mListItem.get(position);
    }

    public void remove(int position){
        this.mListItem.remove(position);
    }

    public void removeFrom(Object object){
        this.mListItem.remove(object);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCard;

        public ViewHolder(CardView vCard) {
            super(vCard);
            this.mCard = vCard;
        }
    }

    @Override
    public AgroAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(AgroAdapter.ViewHolder viewHolder, int position) {
        return;
    }

    @Override
    public int getItemCount() {
        return mListItem.size();
    }

    public int getIndexObject(Object object){
        return mListItem.indexOf(object);
    }
    public void setOnItemMenuToolbarClick(ItemMenuToolbarListener itemMenuToolbarListener){
        this.itemMenuToolbarListener = itemMenuToolbarListener;
    }

    public ItemMenuToolbarListener getOnItemMenuToolbarListener(){
        return this.itemMenuToolbarListener;
    }
}
