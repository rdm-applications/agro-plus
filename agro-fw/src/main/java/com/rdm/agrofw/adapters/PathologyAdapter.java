package com.rdm.agrofw.adapters;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rdm.agrofw.R;
import com.rdm.api.entidades.DoencaBase;

import java.util.List;

/**
 * Created by Robson Da Motta on 16/02/2016.
 */
public class PathologyAdapter extends AgroAdapter {

    public PathologyAdapter(List<DoencaBase> listPathologys) {
        super(listPathologys);
    }

    @Override
    public AgroAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        CardView mCardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fw_card_view, viewGroup, false);

        ViewHolder vh = new ViewHolder(mCardView);
        return vh;
    }

    @Override
    public void onBindViewHolder(AgroAdapter.ViewHolder viewHolder, int position) {
        TextView mText = (TextView) viewHolder.mCard.findViewById(R.id.fwtvItemCardView);
        DoencaBase pathology = (DoencaBase) getItem(position);
        mText.setText(pathology.getPathologyPopularName());
        mText.setClickable(true);
        viewHolder.mCard.setTag(R.string.agro_fw_TAG, pathology);
    }
}
