package com.rdm.agro.controllers;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

/**
 * Created by Robson Da Motta on 12/11/2015.
 */
public class ControllerFragmentQuery {

    protected static final int CLICK_NEXT = 2;
    protected static final int CLICK_RETURN = 1;
    protected static final int INITIAL_INDEX = 0;

    private FloatingActionButton fabReturn;
    private FloatingActionButton fabNext;
    private int stateVisibleFabReturn;
    private int stateVisibleFabNext;
    private int index;
    private static ControllerFragmentQuery instanceController;

    protected ControllerFragmentQuery(){

    }

    protected void controllerIndex(int action){
        if (action == CLICK_NEXT){
            this.index += 1;
        } else {
            this.index -= 1;
        }

        if (this.index <= INITIAL_INDEX) {
            this.index = INITIAL_INDEX;
        }
        if (this.index > INITIAL_INDEX){
            setStateVisibleFabReturn(View.VISIBLE);
        } else {
            setStateVisibleFabReturn(View.INVISIBLE);
        }
        setVisibleButtons();
    }

    public boolean getStateVisibilitFabReturn(){
        return this.stateVisibleFabReturn == View.VISIBLE;
    }

    public boolean getStateVisibilitFabNext(){
        return this.stateVisibleFabNext == View.VISIBLE;
    }

    public void setStateVisibleFabReturn( int visiblestate){
        this.stateVisibleFabReturn = visiblestate;
    }

    public void setStateVisibleFabNext( int visiblestate){
        this.stateVisibleFabNext = visiblestate;
    }

    public void setVisibleButtons(){
        fabReturn.setVisibility(getStateVisibilitFabReturn() ? View.VISIBLE : View.INVISIBLE);
        fabNext.setVisibility(getStateVisibilitFabNext() ? View.VISIBLE : View.INVISIBLE);
    }

    public static ControllerFragmentQuery getInstanceController(FloatingActionButton buttonReturn, FloatingActionButton buttonNext){
        if (instanceController == null){
            instanceController = new ControllerFragmentQuery();
            instanceController.index = INITIAL_INDEX;
        }
        instanceController.setStateVisibleFabReturn(View.INVISIBLE);
        instanceController.setStateVisibleFabNext(View.VISIBLE);
        instanceController.fabReturn = buttonReturn;
        instanceController.fabNext = buttonNext;

        instanceController.setVisibleButtons();

        instanceController.fabReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceController.clickReturn();
            }
        });
        instanceController.fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceController.clickNext();
            }
        });

        return instanceController;
    }

    protected void clickReturn() {
        controllerIndex(CLICK_RETURN);

    }
    protected void clickNext() {
        controllerIndex(CLICK_NEXT);
    }
}
