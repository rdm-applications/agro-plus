package com.rdm.agro.basic;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.rdm.agro.R;

/**
 * Created by Robson Da Motta on 28/10/2015.
 */
public class AgroActivity extends AppCompatActivity  {

    private ActionBarDrawerToggle agroDrawerToggle;
    private DrawerLayout agroDrawer;
    private NavigationView agroNavigationView;
    private Toolbar agroToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /*
    This method is responsible for initialization of components in the View
     */
    public void initComponents(){
        initDrawer();
        initNavigationView();
        initAgroToolbar();
        initDrawerToggle();
    }
    /*
    This method is responsible for initialization of
    DrawerLayout for ActionBarDrawerToggle in the View
     */
    protected void initDrawer(){
        setupDrawerToggle();
    }

    /*
   This method is responsible for initialization of NavigationView in the View
    */
    protected void initNavigationView(){
        if (agroNavigationView != null) {
            setupDrawerContent(agroNavigationView);
        }
    }

    /*
    This method is responsible for initialization of
    Toolbar of the View
    */
    protected void initAgroToolbar(){
        if (agroToolbar != null) {
            setSupportActionBar(agroToolbar);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        }
    }

    /*
    This method is responsible for initialization of
    ActionBarDrawerToggle in the View
    */
    protected void initDrawerToggle(){
        if ((agroDrawer != null) && (agroToolbar != null)){

            agroDrawerToggle = setupDrawerToggle();
            agroDrawerToggle.setDrawerIndicatorEnabled(true);
            agroDrawer.setDrawerListener(agroDrawerToggle);
        }
    }

    /*
    This method return a new instance of ActionBarDrawerToggle for
    use in the DrawerLayout in the View
     */
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, agroDrawer, agroToolbar, R.string.drawer_open, R.string.drawer_close);
    }

    /*
    This method is responsible for definitions of events click of the item menu drawer
     */
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        onItemDrawerSelected(menuItem);
                        return true;
                    }
                });
    }

    public DrawerLayout getAgroDrawer() {
        return agroDrawer;
    }

    public Toolbar getAgroToolbar() {
        return agroToolbar;
    }

    public NavigationView getAgroNavigationView(){
        return agroNavigationView;
    }

    public ActionBarDrawerToggle getAgroDrawerToggle() {
        return agroDrawerToggle;
    }

    public void setAgroDrawer(DrawerLayout drawer){
        this.agroDrawer = drawer;
    }

    public void setToolbar(Toolbar toolbar){
        this.agroToolbar = toolbar;
    }

    public void setAgroNavigationView(NavigationView agroNavigationView) {
        this.agroNavigationView = agroNavigationView;
    }

    /*
    Notify the ActionBarDrawerToggle after create in the View.
   */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (agroDrawerToggle != null) {
            agroDrawerToggle.syncState();
        }
    }

    /*
    Define configuration of ActionBarDrawerToggle in the View.
   */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (agroDrawerToggle != null) {
            agroDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    /*
    Methos for events item selected of menu context
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setupItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    /*
    Define initial state on drawer menu after process.
     */
    public void onItemDrawerSelected(MenuItem menuItem) {
        getAgroDrawerToggle().onOptionsItemSelected(menuItem);

        setupItemSelected(menuItem);
        getAgroDrawer().closeDrawer(GravityCompat.START);
    }

    /*
    Method for events item menu
     */
    protected void setupItemSelected(MenuItem menuItem){
        return;
    }
}
