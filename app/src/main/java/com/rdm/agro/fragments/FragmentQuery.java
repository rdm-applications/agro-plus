package com.rdm.agro.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rdm.agro.R;
import com.rdm.agro.basic.AgroFragment;
import com.rdm.agro.controllers.ControllerFragmentQuery;
import com.rdm.agrofw.adapters.SymptomAdapter;
import com.rdm.agrofw.classes.SwipeRecyclerViewTouch;
import com.rdm.agrofw.core.Modulo;
import com.rdm.agrofw.interfaces.SwipeViewListener;
import com.rdm.api.entidades.SintomaBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robson Da Motta on 06/11/2015.
 */
public class FragmentQuery extends AgroFragment {

    private ControllerFragmentQuery controllerFragmentQuery;
    private FloatingActionButton fabReturn;
    private FloatingActionButton fabNext;

    public static FragmentQuery newInstance() {
        return new FragmentQuery();
    }

    public static FragmentQuery newInstance(Bundle savedInstanceState) {
        FragmentQuery fragment = new FragmentQuery();
        fragment.setArguments(savedInstanceState);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_initiate_queryapp, null);

        if (fragmentView != null) {
            fabReturn = (FloatingActionButton) fragmentView.findViewById(R.id.fab_return);
            fabNext = (FloatingActionButton) fragmentView.findViewById(R.id.fab_next);

            String parameterKey = getString(R.string.agro_fw_modulo);

            if ((getArguments() != null) && (getArguments().containsKey(parameterKey))) {
                fragmentView = inflater.inflate(R.layout.fw_frame_recycle, null);
                RecyclerView mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvRecyclerView);

                mRecyclerView.setHasFixedSize(true);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                Modulo modulo = (Modulo) getArguments().getSerializable(parameterKey);
                ArrayList<SintomaBase> listSymptons = modulo.getGroupSymptom(4);
                final SymptomAdapter mAdapter = new SymptomAdapter((List<SintomaBase>) listSymptons.clone());

                mRecyclerView.setAdapter(mAdapter);

                SwipeRecyclerViewTouch swipeTouchListener =
                        new SwipeRecyclerViewTouch(mRecyclerView,
                                new SwipeViewListener() {
                                    @Override
                                    public boolean doSwipe(int position) {
                                        return true;
                                    }

                                    @Override
                                    public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                        for (int position : reverseSortedPositions) {
                                            mAdapter.remove(position);
                                            mAdapter.notifyItemRemoved(position);
                                        }
                                        mAdapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                        for (int position : reverseSortedPositions) {
                                            mAdapter.remove(position);
                                            mAdapter.notifyItemRemoved(position);
                                        }
                                        mAdapter.notifyDataSetChanged();
                                    }
                                });

                mRecyclerView.addOnItemTouchListener(swipeTouchListener);
            }

            controllerFragmentQuery = ControllerFragmentQuery.getInstanceController(fabReturn, fabNext);
            fabNext.callOnClick();
            return fragmentView;
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }


    }
}
