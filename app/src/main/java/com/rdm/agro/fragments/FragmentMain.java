package com.rdm.agro.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rdm.agro.R;
import com.rdm.agro.basic.AgroFragment;

/**
 * Created by Robson Da Motta on 05/11/2015.
 */
public class FragmentMain extends AgroFragment {

    public static FragmentMain newInstance() {
        return new FragmentMain();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_homeapp, null);

        if (fragmentView != null) {
            return fragmentView;
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }
}
