package com.rdm.agro.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.rdm.agro.R;
import com.rdm.agro.basic.AgroFragment;
import com.rdm.agrofw.adapters.PathologyDetailAdapter;
import com.rdm.agrofw.classes.SwipeRecyclerViewTouch;
import com.rdm.agrofw.core.Modulo;
import com.rdm.agrofw.interfaces.ItemMenuToolbarListener;
import com.rdm.agrofw.interfaces.SwipeViewListener;
import com.rdm.api.entidades.DoencaBase;

import java.util.ArrayList;

/**
 * Created by Robson Da Motta on 05/11/2015.
 */
public class FragmentAbout extends AgroFragment {

    public static FragmentAbout newInstance() {
        return new FragmentAbout();
    }

    public static FragmentAbout newInstance(Bundle savedInstanceState) {
        FragmentAbout fragment = new FragmentAbout();
        fragment.setArguments(savedInstanceState);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_aboutapp, null);

        if (fragmentView != null) {

            String parameterKey = getString(R.string.agro_fw_modulo);

            if ((getArguments() != null) && (getArguments().containsKey(parameterKey))) {
                fragmentView = inflater.inflate(R.layout.fw_frame_recycle, null);
                RecyclerView mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.rvRecyclerView);

                mRecyclerView.setHasFixedSize(true);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                ArrayList<DoencaBase> listPathologys = (ArrayList<DoencaBase>) getArguments().getSerializable(parameterKey);
                final PathologyDetailAdapter mAdapter = new PathologyDetailAdapter((ArrayList<DoencaBase>)listPathologys.clone());

                mAdapter.setOnItemMenuToolbarClick(new ItemMenuToolbarListener() {
                    @Override
                    public boolean onItemMenuToolbarClick(MenuItem menuItem, Object objectItem) {
                        DoencaBase pathology = (DoencaBase) objectItem;

                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                        alert.setTitle(menuItem.getTitle());
                        alert.setNeutralButton("OK", null);

                        if (menuItem.getItemId() == R.id.action_detail_mn_toolbar){
                            alert.setMessage(Modulo.showDetail(objectItem));
                            alert.show();

                        } else if (menuItem.getItemId() == R.id.action_symptom_mn_toolbar){
                            alert.setMessage(Modulo.showDetail(pathology.getSintomas()));
                            alert.show();

                        } else if (menuItem.getItemId() == R.id.action_diagnostic_mn_toolbar){
                            alert.setMessage(Modulo.showDetail(pathology.getDiagnosticos()));
                            alert.show();
                        }
                        return false;
                    }
                });
                mRecyclerView.setAdapter(mAdapter);

                SwipeRecyclerViewTouch swipeTouchListener =
                        new SwipeRecyclerViewTouch(mRecyclerView,
                                new SwipeViewListener() {
                                    @Override
                                    public boolean doSwipe(int position) {
                                        return true;
                                    }

                                    @Override
                                    public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                        for (int position : reverseSortedPositions) {
                                            mAdapter.remove(position);
                                            mAdapter.notifyItemRemoved(position);
                                        }
                                        mAdapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                        for (int position : reverseSortedPositions) {
                                            mAdapter.remove(position);
                                            mAdapter.notifyItemRemoved(position);
                                        }
                                        mAdapter.notifyDataSetChanged();
                                    }
                                });

                mRecyclerView.addOnItemTouchListener(swipeTouchListener);
            }


            return fragmentView;
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }
}
