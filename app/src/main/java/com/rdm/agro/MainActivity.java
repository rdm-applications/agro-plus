package com.rdm.agro;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.rdm.agro.basic.AgroActivity;
import com.rdm.agro.basic.AgroFragment;
import com.rdm.agro.fragments.FragmentAbout;
import com.rdm.agro.fragments.FragmentMain;
import com.rdm.agro.fragments.FragmentQuery;
import com.rdm.agrofw.core.Modulo;
import com.rdm.api.entidades.DoencaBase;

import java.util.ArrayList;

public class MainActivity extends AgroActivity {

    /*
      This object is the Content for uses on events from layout,
      or the main content view where fragments are loaded.
     */
    FrameLayout frameContent;
    Modulo modulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initComponents();
    }

    /*
    This method is responsible for initialization of components in the view
     */
    @Override
    public void initComponents() {

        setAgroDrawer((DrawerLayout) findViewById(R.id.drawer_layout));
        setToolbar((Toolbar) findViewById(R.id.toolbar));
        setAgroNavigationView((NavigationView) findViewById(R.id.nv_menu_drawer_main));

        frameContent = (FrameLayout) findViewById(R.id.fl_Content);

        modulo = Modulo.newModulo();
        modulo.inicializar(getApplicationContext());
        modulo.prepare();

        defineFragmentVisible(FragmentMain.newInstance());

        super.initComponents();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void setupItemSelected(MenuItem menuItem) {
        Bundle bundleParam = new Bundle();
        String tagParam = getString(R.string.agro_fw_modulo);



        int id = menuItem.getItemId();

        if (id == R.id.action_drawer_home) {
            defineFragmentVisible(FragmentMain.newInstance());
            return;
        } else if (id == R.id.action_drawer_initiate_query || id == R.id.action_initiate_query) {
            bundleParam.putSerializable(tagParam, modulo);
            defineFragmentVisible(FragmentQuery.newInstance(bundleParam));
            return;
        } else if (id == R.id.action_drawer_settings || id == R.id.action_settings) {
            return;
        } else if (id == R.id.action_drawer_about_app || id == R.id.action_about_app) {
            bundleParam.putSerializable(tagParam, (ArrayList<DoencaBase>) modulo.getListPathologys());
            defineFragmentVisible(FragmentAbout.newInstance(bundleParam));
            return;
        }
    }

    protected void defineFragmentVisible(AgroFragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_Content, fragment)
                .addToBackStack(null)
                .commit();
    }
}
